package com.example.practica03s2java;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import Modelo.AlumnosDb;


public class AlumnoAlta extends AppCompatActivity {

    private Button btnGuardar, btnRegresar;
    private Alumno alumno;
    private EditText txtNombre, txtMatricula, txtGrado;
    private ImageView imgAlumno;
    private TextView lblImagen;
    private String carrera;
    private int posicion;
    private AlumnosDb alumnosDb;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_alumno_alta);
        btnGuardar = (Button) findViewById(R.id.btnSalir);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        txtMatricula = (EditText) findViewById(R.id.txtMatricula);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtGrado = (EditText) findViewById(R.id.txtGrado);
        imgAlumno = (ImageView) findViewById(R.id.imgAlumno);

        Bundle bundle = getIntent().getExtras();
        alumno = (Alumno) bundle.getSerializable("alumno");
        posicion = bundle.getInt("posicion", posicion);

        if(posicion >= 0 && alumno!=null){
            txtMatricula.setText(alumno.getTextMatricula());
            txtNombre.setText(alumno.getTextNombre());
            txtGrado.setText(alumno.getTextCarrera());
            imgAlumno.setImageResource(alumno.getImageId());
        }

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(posicion >= 0 && alumno!=null){
                    // Modificar Alumno
                    alumno.setTextMatricula(txtMatricula.getText().toString());
                    alumno.setTextNombre(txtNombre.getText().toString());
                    alumno.setTextCarrera(txtGrado.getText().toString());

                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setTextMatricula(alumno.getTextMatricula());
                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setTextNombre(alumno.getTextNombre());
                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setTextCarrera(alumno.getTextCarrera());

                    alumnosDb = new AlumnosDb(getApplicationContext());
                    alumnosDb.updateAlumno(alumno);

                    Toast.makeText(getApplicationContext(), "Se modifico con exito ", Toast.LENGTH_SHORT).show();
                } else {
                    // Agregar un nuevo alumno
                    String matricula = txtMatricula.getText().toString();
                    String nombre = txtNombre.getText().toString();
                    String grado = txtGrado.getText().toString();

                    if (matricula.isEmpty() || nombre.isEmpty() || grado.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Faltó capturar datos", Toast.LENGTH_SHORT).show();
                        txtMatricula.requestFocus();
                    } else {
                        alumno = new Alumno();
                        alumno.setTextMatricula(matricula);
                        alumno.setTextNombre(nombre);
                        alumno.setTextCarrera(grado);
                        alumno.setImageId(R.drawable.us01);

                        alumnosDb = new AlumnosDb(getApplicationContext());
                        alumnosDb.insertAlumno(alumno);

                        ((Aplicacion) getApplication()).getAlumnos().add(alumno);

                        ((Aplicacion) getApplication()).getAdaptador().notifyDataSetChanged(); // Notificar al adaptador del cambio en los datos

                        setResult(Activity.RESULT_OK);
                        finish();

                        Toast.makeText(getApplicationContext(), "Se guardó con éxito", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    /*private boolean validar(){
        boolean exito = true;
        Log.d("nombre","validar"+txtNombre.getText());
        if(txtNombre.getText().toString().equals("")) exito=false;
        if(txtMatricula.getText().toString().equals("")) exito=false;
        if(txtGrado.getText().toString().equals("")) exito=false;

        return exito;
    }*/

}
