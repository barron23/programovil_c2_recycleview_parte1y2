/* package com.example.practica03s2java;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AdapterAlumno extends BaseAdapter implements Filterable {
    private Context mContext;
    private List<alumnoItem> mData;
    private List<alumnoItem> mFilteredData;

    public AdapterAlumno(Context context, List<alumnoItem> data) {
        //(Context context, TIPO resource,List<alumnoItem> data)
        //super(context, resource, items);
        mContext = context;
        mData = data;
        mFilteredData = new ArrayList<>(data);
    }

    @Override
    public int getCount() {
        return mFilteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return mFilteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.alumno_item, parent, false);

            holder = new ViewHolder();
            holder.imgFoto = convertView.findViewById(R.id.imgFoto);
            holder.lblMatricula = convertView.findViewById(R.id.lblMatricula);
            holder.lblNombre = convertView.findViewById(R.id.lblNombre);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        alumnoItem item = mFilteredData.get(position);

        holder.imgFoto.setImageResource(item.getImageId());
        holder.lblMatricula.setText(item.getTextMatricula());
        holder.lblNombre.setText(item.getTextNombre());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<alumnoItem> filteredList = new ArrayList<>();

                if (constraint == null || constraint.length() == 0) {
                    filteredList.addAll(mData);
                } else {
                    String filterPattern = constraint.toString().toLowerCase().trim();
                    for (alumnoItem item : mData) {
                        if (item.getTextMatricula().toLowerCase().contains(filterPattern) ||
                                item.getTextNombre().toLowerCase().contains(filterPattern)) {
                            filteredList.add(item);
                        }
                    }
                }

                results.values = filteredList;
                results.count = filteredList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mFilteredData.clear();
                mFilteredData.addAll((List<alumnoItem>) results.values);
                notifyDataSetChanged();
            }
        };
    }

    static class ViewHolder {
        ImageView imgFoto;
        TextView lblMatricula;
        TextView lblNombre;
    }
} */